﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Present_Connection.Startup))]
namespace Present_Connection
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
