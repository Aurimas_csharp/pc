namespace Present_Connection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migr : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Darbuotojai",
                c => new
                    {
                        DarbuotojaiId = c.Int(nullable: false, identity: true),
                        ImonesId = c.Int(nullable: false),
                        Pareigos = c.String(),
                        DarbuotojoVardas = c.String(),
                        DarbuotojoPavarde = c.String(),
                        DarbuotojoEmail = c.String(),
                        DarbuotojoTel = c.String(),
                        RegistracijosData = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.DarbuotojaiId)
                .ForeignKey("dbo.Imones", t => t.ImonesId)
                .Index(t => t.ImonesId);
            
            CreateTable(
                "dbo.Imones",
                c => new
                    {
                        ImonesId = c.Int(nullable: false, identity: true),
                        ImonesPavadinimas = c.String(),
                        Aprasymas = c.String(),
                        Imones_sukurimo_data = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ImonesId);
            
            CreateTable(
                "dbo.Kompetencija",
                c => new
                    {
                        KompetencijaId = c.Int(nullable: false, identity: true),
                        Pavadinimas = c.String(),
                        Aprasymas = c.String(),
                        ImonesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KompetencijaId)
                .ForeignKey("dbo.Imones", t => t.ImonesId)
                .Index(t => t.ImonesId);
            
            CreateTable(
                "dbo.Vertinimas",
                c => new
                    {
                        VertinimasId = c.Int(nullable: false, identity: true),
                        Vertinimas_pazymys = c.Int(nullable: false),
                        Sukurimo_data = c.DateTime(nullable: false),
                        DarbuotojaiId = c.Int(nullable: false),
                        KompetencijaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.VertinimasId)
                .ForeignKey("dbo.Darbuotojai", t => t.DarbuotojaiId)
                .ForeignKey("dbo.Kompetencija", t => t.KompetencijaId)
                .Index(t => t.DarbuotojaiId)
                .Index(t => t.KompetencijaId);
            
            CreateTable(
                "dbo.ImonesIrSkaiciai",
                c => new
                    {
                        ImonesIrSkaiciaiId = c.Int(nullable: false, identity: true),
                        ImonesPav = c.String(),
                        Kompetenciju_skaicius = c.Int(nullable: false),
                        darbuotoju_skaicius = c.Int(nullable: false),
                        Imones_ImonesId = c.Int(),
                    })
                .PrimaryKey(t => t.ImonesIrSkaiciaiId)
                .ForeignKey("dbo.Imones", t => t.Imones_ImonesId)
                .Index(t => t.Imones_ImonesId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ImonesIrSkaiciai", "Imones_ImonesId", "dbo.Imones");
            DropForeignKey("dbo.Vertinimas", "KompetencijaId", "dbo.Kompetencija");
            DropForeignKey("dbo.Vertinimas", "DarbuotojaiId", "dbo.Darbuotojai");
            DropForeignKey("dbo.Kompetencija", "ImonesId", "dbo.Imones");
            DropForeignKey("dbo.Darbuotojai", "ImonesId", "dbo.Imones");
            DropIndex("dbo.ImonesIrSkaiciai", new[] { "Imones_ImonesId" });
            DropIndex("dbo.Vertinimas", new[] { "KompetencijaId" });
            DropIndex("dbo.Vertinimas", new[] { "DarbuotojaiId" });
            DropIndex("dbo.Kompetencija", new[] { "ImonesId" });
            DropIndex("dbo.Darbuotojai", new[] { "ImonesId" });
            DropTable("dbo.ImonesIrSkaiciai");
            DropTable("dbo.Vertinimas");
            DropTable("dbo.Kompetencija");
            DropTable("dbo.Imones");
            DropTable("dbo.Darbuotojai");
        }
    }
}
