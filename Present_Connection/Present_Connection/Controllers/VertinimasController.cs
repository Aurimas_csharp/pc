﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Present_Connection.Models;

namespace Present_Connection.Controllers
{
    public class VertinimasController : Controller
    {
        private PC_contextLast db = new PC_contextLast();

        // GET: Vertinimas
        public ActionResult Index()
        {
            var kompetVertinimas = db.KompetVertinimas.Include(v => v.darbuotojas).Include(v => v.kompetencija);
            return View(kompetVertinimas.ToList());
        }

        // GET: Vertinimas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vertinimas vertinimas = db.KompetVertinimas.Find(id);
            var naujas = db.Kompetencijos.Where(p => p.KompetencijaId == vertinimas.KompetencijaId).ToList();
            string Pavadinimas = null;
            if (naujas.Count != 0)
            {
                Pavadinimas = naujas[0].Pavadinimas;
            }


            if (vertinimas == null)
            {
                return HttpNotFound();
            }
            return View(vertinimas);
        }

        // GET: Vertinimas/Create
        public ActionResult Create()
        {
            ViewBag.DarbuotojaiId = new SelectList(db.Darbuotojai, "DarbuotojaiId", "Pareigos");
            ViewBag.KompetencijaId = new SelectList(db.Kompetencijos, "KompetencijaId", "Pavadinimas");
            return View();
        }

        // POST: Vertinimas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VertinimasId,Vertinimas_pazymys,Sukurimo_data,DarbuotojaiId,KompetencijaId")] Vertinimas vertinimas)
        {
            if (ModelState.IsValid)
            {
                db.KompetVertinimas.Add(vertinimas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DarbuotojaiId = new SelectList(db.Darbuotojai, "DarbuotojaiId", "Pareigos", vertinimas.DarbuotojaiId);
            ViewBag.KompetencijaId = new SelectList(db.Kompetencijos, "KompetencijaId", "Pavadinimas", vertinimas.KompetencijaId);
            return View(vertinimas);
        }

        // GET: Vertinimas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vertinimas vertinimas = db.KompetVertinimas.Find(id);
            if (vertinimas == null)
            {
                return HttpNotFound();
            }
            ViewBag.DarbuotojaiId = new SelectList(db.Darbuotojai, "DarbuotojaiId", "Pareigos", vertinimas.DarbuotojaiId);
            ViewBag.KompetencijaId = new SelectList(db.Kompetencijos, "KompetencijaId", "Pavadinimas", vertinimas.KompetencijaId);
            return View(vertinimas);
        }

        // POST: Vertinimas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VertinimasId,Vertinimas_pazymys,Sukurimo_data,DarbuotojaiId,KompetencijaId")] Vertinimas vertinimas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vertinimas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DarbuotojaiId = new SelectList(db.Darbuotojai, "DarbuotojaiId", "Pareigos", vertinimas.DarbuotojaiId);
            ViewBag.KompetencijaId = new SelectList(db.Kompetencijos, "KompetencijaId", "Pavadinimas", vertinimas.KompetencijaId);
            return View(vertinimas);
        }

        // GET: Vertinimas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vertinimas vertinimas = db.KompetVertinimas.Find(id);
            if (vertinimas == null)
            {
                return HttpNotFound();
            }
            return View(vertinimas);
        }

        // POST: Vertinimas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vertinimas vertinimas = db.KompetVertinimas.Find(id);
            db.KompetVertinimas.Remove(vertinimas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
