﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Present_Connection.Models;

namespace Present_Connection.Controllers
{
    public class KompetencijasController : Controller
    {
        private PC_contextLast db = new PC_contextLast();

        // GET: Kompetencijas
        public ActionResult Index(string SortOrder)
        {
            ViewBag.Imone = string.IsNullOrEmpty(SortOrder) ? "Imones_desc" : "";
            
            var kompetencijos = db.Kompetencijos.Include(k => k.imone);
            var Kompetencijos = kompetencijos.ToList();
            switch (SortOrder)
            {
                case "Imones_desc":

                    Kompetencijos = Kompetencijos.OrderByDescending(s => s.imone.ImonesPavadinimas).ToList();
                    break;
                default:
                    Kompetencijos = Kompetencijos.OrderBy(s => s.imone.ImonesPavadinimas).ToList();
                    break;
            }




            return View(Kompetencijos.ToList());
        }

        // GET: Kompetencijas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kompetencija kompetencija = db.Kompetencijos.Find(id);
            var kompetencijos_imone = db.Imones.Where(p => p.ImonesId == id).ToList();
            kompetencija.imone = kompetencijos_imone[0];
            if (kompetencija == null)
            {
                return HttpNotFound();
            }
            return View(kompetencija);
        }

        // GET: Kompetencijas/Create
        public ActionResult Create()
        {
            ViewBag.ImonesId = new SelectList(db.Imones, "ImonesId", "ImonesPavadinimas");
            return View();
        }

        // POST: Kompetencijas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KompetencijaId,Pavadinimas,Aprasymas,ImonesId")] Kompetencija kompetencija)
        {
            if (ModelState.IsValid)
            {
                db.Kompetencijos.Add(kompetencija);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ImonesId = new SelectList(db.Imones, "ImonesId", "ImonesPavadinimas", kompetencija.ImonesId);
            return View(kompetencija);
        }

        // GET: Kompetencijas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kompetencija kompetencija = db.Kompetencijos.Find(id);
            if (kompetencija == null)
            {
                return HttpNotFound();
            }
            ViewBag.ImonesId = new SelectList(db.Imones, "ImonesId", "ImonesPavadinimas", kompetencija.ImonesId);
            return View(kompetencija);
        }

        // POST: Kompetencijas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KompetencijaId,Pavadinimas,Aprasymas,ImonesId")] Kompetencija kompetencija)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kompetencija).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ImonesId = new SelectList(db.Imones, "ImonesId", "ImonesPavadinimas", kompetencija.ImonesId);
            return View(kompetencija);
        }

        // GET: Kompetencijas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kompetencija kompetencija = db.Kompetencijos.Find(id);
            if (kompetencija == null)
            {
                return HttpNotFound();
            }
            return View(kompetencija);
        }

        // POST: Kompetencijas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kompetencija kompetencija = db.Kompetencijos.Find(id);
            db.Kompetencijos.Remove(kompetencija);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
