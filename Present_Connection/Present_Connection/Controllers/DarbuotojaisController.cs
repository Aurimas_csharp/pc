﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Present_Connection.Models;

namespace Present_Connection.Controllers
{
    public class DarbuotojaisController : Controller
    {
        private PC_contextLast db = new PC_contextLast();

        // GET: Darbuotojais
        public ActionResult Index(string SortOrder)
        {

            ViewBag.Imone = string.IsNullOrEmpty(SortOrder) ? "Imones_desc" : "";


            var Imonikes = db.Darbuotojai.Include(d => d.imone);
            var imonikes = Imonikes.ToList();
           

            switch (SortOrder)
            {
                case "Imones_desc":

                    imonikes = imonikes.OrderByDescending(s => s.imone.ImonesPavadinimas).ToList();
                    break;
                default:
                    imonikes = imonikes.OrderBy(s => s.imone.ImonesPavadinimas).ToList();
                    break;
            }
            Darbuotojai NaujasDarb = new Darbuotojai();
            List<DarbuotojaiViewmodel> stats = new List<DarbuotojaiViewmodel>();
            for(int i = 0;  i < imonikes.Count; i++)
            {



            }





            return View(imonikes.ToList());
        }

        // GET: Darbuotojais/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Darbuotojai darbuotojai = db.Darbuotojai.Find(id);
            var naujas = db.Imones.Where(p => p.ImonesId == darbuotojai.ImonesId).ToList();
            string Pavadinimas = null;
            if (naujas.Count != 0)
            {
                Pavadinimas = naujas[0].ImonesPavadinimas;
            }
            ViewBag.Pav = Pavadinimas;

            if (darbuotojai == null)
            {
                return HttpNotFound();
            }
            return View(darbuotojai);
        }

        // GET: Darbuotojais/Create
        public ActionResult Create()
        {
            ViewBag.ImonesId = new SelectList(db.Imones, "ImonesId", "ImonesPavadinimas");
            return View();
        }

        // POST: Darbuotojais/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DarbuotojaiId,DarbuotojoVardas,DarbuotojoPavarde,DarbuotojoEmail,DarbuotojoTel,RegistracijosData,ImonesId,Pareigos")] Darbuotojai darbuotojai)
        {
            if (ModelState.IsValid)
            {
                db.Darbuotojai.Add(darbuotojai);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ImonesId = new SelectList(db.Imones, "ImonesId", "ImonesPavadinimas", darbuotojai.ImonesId);
            return View(darbuotojai);
        }

        // GET: Darbuotojais/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Darbuotojai darbuotojai = db.Darbuotojai.Find(id);
            if (darbuotojai == null)
            {
                return HttpNotFound();
            }
            ViewBag.ImonesId = new SelectList(db.Imones, "ImonesId", "ImonesPavadinimas", darbuotojai.ImonesId);
            return View(darbuotojai);
        }

        // POST: Darbuotojais/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DarbuotojaiId,DarbuotojoVardas,DarbuotojoPavarde,DarbuotojoEmail,DarbuotojoTel,RegistracijosData,ImonesId,Pareigos")] Darbuotojai darbuotojai)
        {
            if (ModelState.IsValid)
            {
                db.Entry(darbuotojai).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ImonesId = new SelectList(db.Imones, "ImonesId", "ImonesPavadinimas", darbuotojai.ImonesId);
            return View(darbuotojai);
        }

        // GET: Darbuotojais/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Darbuotojai darbuotojai = db.Darbuotojai.Find(id);
            if (darbuotojai == null)
            {
                return HttpNotFound();
            }
            return View(darbuotojai);
        }

        // POST: Darbuotojais/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Darbuotojai darbuotojai = db.Darbuotojai.Find(id);
            db.Darbuotojai.Remove(darbuotojai);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
