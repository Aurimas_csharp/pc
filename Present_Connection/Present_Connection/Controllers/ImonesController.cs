﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Present_Connection.Models;

namespace Present_Connection.Controllers
{
    public class ImonesController : Controller
    {
        private PC_contextLast db = new PC_contextLast();

        // GET: Imones
        public ActionResult Index()
        {
            var Imones = db.Imones.ToList(); //Kad suktu for'ą
            Imones imone = new Imones();  
            List<int> KiekDarb = new List<int>();
            List<int> KiekKomp = new List<int>();
            int sk = 0;
            List<ImonesIrSkaiciai> stats = new List<ImonesIrSkaiciai>();

            for (int i = 0; i < Imones.Count; i++)
            { 
                sk = Imones[i].ImonesId;
                var AtrinktiDarb = db.Darbuotojai.Where(z => z.ImonesId == sk).ToList();
                var AtrinktosKomp = db.Kompetencijos.Where(z => z.ImonesId == sk).ToList();

                ImonesIrSkaiciai naujas = new ImonesIrSkaiciai { ImonesIrSkaiciaiId = Imones[i].ImonesId, ImonesPav = Imones[i].ImonesPavadinimas, darbuotoju_skaicius = AtrinktiDarb.Count, Kompetenciju_skaicius = AtrinktosKomp.Count };

                stats.Add(naujas);
            }

            imone.Stats = stats;

            return View(imone);
        }

        // GET: Imones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imones imones = db.Imones.Find(id);

            var Imones_darbuotojai = db.Darbuotojai.Where(p => p.ImonesId == id).ToList();
            imones.Darbuotojai = Imones_darbuotojai;
            var Imones_kompetencijos = db.Kompetencijos.Where(p => p.ImonesId == id).ToList();
            imones.Kompetencijos = Imones_kompetencijos;
            if (imones == null)
            {
                return HttpNotFound();
            }
            return View(imones);
        }

        // GET: Imones/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Imones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ImonesId,ImonesPavadinimas,Aprasymas,Imones_sukurimo_data")] Imones imones)
        {
            if (ModelState.IsValid)
            {
                db.Imones.Add(imones);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(imones);
        }

        // GET: Imones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imones imones = db.Imones.Find(id);
            if (imones == null)
            {
                return HttpNotFound();
            }
            return View(imones);
        }

        // POST: Imones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ImonesId,ImonesPavadinimas,Aprasymas,Imones_sukurimo_data")] Imones imones)
        {
            if (ModelState.IsValid)
            {
                db.Entry(imones).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(imones);
        }

        // GET: Imones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imones imones = db.Imones.Find(id);
            if (imones == null)
            {
                return HttpNotFound();
            }
            return View(imones);
        }

        // POST: Imones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Imones imones = db.Imones.Find(id);
            db.Imones.Remove(imones);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
