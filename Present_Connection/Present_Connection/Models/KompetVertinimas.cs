﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Present_Connection.Models
{
    public class Vertinimas
    {
        public int VertinimasId { get; set; }
        [DisplayName("Pažymys")]
        public int Vertinimas_pazymys { get; set; }
        private DateTime _Imones_sukurimo_data = DateTime.Now;
        public DateTime Sukurimo_data
        {
            get { return _Imones_sukurimo_data; }
            set { { _Imones_sukurimo_data = value; } }
        }

        public Darbuotojai darbuotojas { get; set; }
        public Kompetencija kompetencija { get; set; }
        public int DarbuotojaiId { get; set; }
        public int KompetencijaId { get; set; }
    }
}