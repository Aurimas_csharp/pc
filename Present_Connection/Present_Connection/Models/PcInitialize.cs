﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Present_Connection.Models
{
    public class PcInitialize : DropCreateDatabaseIfModelChanges<PC_contextLast>
    {
        protected override void Seed(PC_contextLast context)
        {
            var Imones = new List<Imones>
            {
                new Imones {ImonesPavadinimas = "Express pizza", Imones_sukurimo_data = DateTime.Parse("2014-01-02") }
,               new Imones {ImonesPavadinimas = "Delete_me_pls", Imones_sukurimo_data = DateTime.Parse("2014-01-02") }
            };
            foreach (var temp in Imones)
            {
                context.Imones.Add(temp);
            }
            context.SaveChanges();
            ///////////////////////////////////////////////////////////////
            var Darbuotojai = new List<Darbuotojai>
            {
                new Darbuotojai {DarbuotojoVardas = "Linas", RegistracijosData = DateTime.Parse("2012-05-06" ) },
                new Darbuotojai {DarbuotojoVardas = "Stasys", RegistracijosData = DateTime.Parse("2011-02-12") }
            };
            foreach (var temp in Darbuotojai)
            {
                context.Darbuotojai.Add(temp);
            }
            context.SaveChanges();
            /////////////////////////////////////////////////////////////
            var Kompetencija = new List<Kompetencija>
            {
                new Kompetencija {Pavadinimas = "Junior", Aprasymas = "Jaunasis programuotojas" }
,               new Kompetencija {Pavadinimas = "Senior", Aprasymas = "Patyres programuotojas" }
            };
            foreach (var temp in Kompetencija)
            {
                context.Kompetencijos.Add(temp);
            }
            context.SaveChanges();
            //////////////////////////////////////////////////////////////
            var Kompet_vertinimas = new List<Vertinimas>
            {
                new Vertinimas { DarbuotojaiId = 1, KompetencijaId = 1, Vertinimas_pazymys = 5,Sukurimo_data = DateTime.Parse("2014-01-02") }
,               new Vertinimas {DarbuotojaiId = 2, KompetencijaId = 2, Vertinimas_pazymys = 10, Sukurimo_data = DateTime.Parse("2014-01-02") }
            };
            foreach (var temp in Kompet_vertinimas)
            {
                context.KompetVertinimas.Add(temp);
            }
            context.SaveChanges();


        }
    }
}