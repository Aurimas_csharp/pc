﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Present_Connection.Models
{
    public class Kompetencija
    {
        public int KompetencijaId { get; set; }
        [DisplayName("Kompetencija")]
        public string Pavadinimas { get; set; }
        [DisplayName("Aprašymas")]
        public string Aprasymas { get; set; }

        public List<Vertinimas> Vertinimas {get;set;}
        public Imones imone { get; set; }
        public int ImonesId { get; set; }
    }

}