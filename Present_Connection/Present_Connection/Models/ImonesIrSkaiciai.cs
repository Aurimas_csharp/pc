﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Present_Connection.Models
{
    public class ImonesIrSkaiciai
    {
        public int ImonesIrSkaiciaiId { get; set; }
        public string ImonesPav { get; set; }
        public int Kompetenciju_skaicius { get; set; }
        public int darbuotoju_skaicius {get; set;}
    }
}