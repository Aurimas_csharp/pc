﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Present_Connection.Models
{
    public class DarbuotojaiViewmodel
    {
        public int DarbuotojaiViewmodelId { get; set; }
        public string DarbuotojoVardas { get; set; }
        public string DarbuotojoKompetencija { get; set; }
        public int PaskutinisVertinimas { get; set; }

    }
}