﻿using
    System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Present_Connection.Models
{
    public class PC_contextLast: DbContext
    { public DbSet<Imones> Imones { get; set; }
      public DbSet<Darbuotojai> Darbuotojai { get; set; }
      public DbSet<Kompetencija> Kompetencijos { get; set; }
      public DbSet<Vertinimas> KompetVertinimas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}