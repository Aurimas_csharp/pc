﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Present_Connection.Models
{
    public class Darbuotojai
    {
        public int DarbuotojaiId { get; set; }
        public int ImonesId { get; set; }

        public string Pareigos { get; set; }
        public string DarbuotojoVardas { get; set; }
        public string DarbuotojoPavarde { get; set; }
        [DisplayName("El. paštas")]
        public string DarbuotojoEmail { get; set; }
        [DisplayName("Tel. numeris")]
        public string DarbuotojoTel { get; set; }
        private DateTime _Imones_sukurimo_data = DateTime.Now;
        public DateTime RegistracijosData  { 
            get { return _Imones_sukurimo_data; }
    set { { _Imones_sukurimo_data = value; } }
      }

        public Imones imone { get; set; }
        public List <Vertinimas> Vertinimai { get; set; }

        public string VardPavard
        {
            get { return DarbuotojoVardas + " " + DarbuotojoPavarde; }
        }

     //   public DarbuotojaiViewmodel stats { get; set; }

    }
}