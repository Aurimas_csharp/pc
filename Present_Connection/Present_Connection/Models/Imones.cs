﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;
namespace Present_Connection.Models
{
    public class Imones
    {

        public int ImonesId { get; set; }
        [DisplayName("Įmonės pavadinimas")]
        public string ImonesPavadinimas { get; set; }
        [DisplayName("Įmonės aprašymas")]
        public string Aprasymas { get; set; }
        private DateTime _Imones_sukurimo_data = DateTime.Now;
        [DisplayName("Įmonės sukūrimo data")]
        public DateTime Imones_sukurimo_data { 
            get { return _Imones_sukurimo_data; }
          set { { _Imones_sukurimo_data = value; } }
      }

        
        public List <Darbuotojai> Darbuotojai { get; set; }
        public List<Kompetencija> Kompetencijos { get; set; }
        public List<ImonesIrSkaiciai> Stats { get; set; }
    }
}